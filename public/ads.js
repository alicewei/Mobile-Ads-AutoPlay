// add script
var mobileCKAds = function(adsInfo){

    // protect
    if (!adsInfo['adsImgBg']){
        console.log('no adsImgBg / adsImgBgs')
        return;
    }

    if (!adsInfo['adsUrl']){
        console.log('no adsUrl')
        return;
    }


    var adsType;
    var checkAdsType = function(){
        // adsType = adsInfo['adsYtVideoId'] ? 'videoAds' : 'imgAds';

        if(adsInfo['adsImgBg'] && adsInfo['adsUrl']){
            if(adsInfo['adsYtVideoId'] && !adsInfo['adsImgBgs']){
                adsType = 'videoAds';
            }else if(!adsInfo['adsYtVideoId'] && adsInfo['adsImgBgs']){
                adsType = 'imgAds';
            }else{
                console.log('which Ad Type?');
                return;
            }
        }else if(!adsInfo['adsImgBg']){
            console.log('no adsImgBg')
            return;
        }else if(!adsInfo['adsUrl']){
            console.log('no adsUrl')
            return;
        }else{
            console.log('you need more Ad source');
            return;
        }

        console.log('adsType =', adsType)
    }


    var adsImgBgs = adsInfo['adsImgBgs'],
        adsImgBg = adsInfo['adsImgBg'],
        adsUrl = adsInfo['adsUrl'],
        adsYtVideoId = adsInfo['adsYtVideoId'];

    // variable obj
    var creatAdsVars = {};

    var creatAdsLayout = function(){
        // creat layout string
        var mobileAdsLayout = '';

        if(adsType == 'videoAds'){
            mobileAdsLayout += '<div class="ads_wraper">\n'
            + '<div class="ads_content">\n'
            + '<div class="btn_ads_display"><span></span></div>\n'
            + '<div class="ads_img"></div>\n'
            + '<div class="adsYtPlayerWrap">\n'
            + '<div id="adsYtPlayer"></div>\n'
            + '</div>\n'
            + '<div class="btn_ads_close"><span></span></div>\n'
            + '</div>\n'
            + '</div>';
        }else{
            mobileAdsLayout += '<div class="ads_wraper">\n'
            + '<div class="ads_content">\n'
            + '<div class="btn_ads_display"><span></span></div>\n'
            + '<div class="ads_img"></div>\n'
            + '<div class="adsYtPlayerWrap"></div>\n'
            + '<div class="btn_ads_close"><span></span></div>\n'
            + '</div>\n'
            + '</div>';
        }

        // add div
        var mobileCKAdsContent = document.createElement('div');
        mobileCKAdsContent.innerHTML = mobileAdsLayout;
        document.getElementById('mobileCKAds').parentNode.insertBefore(mobileCKAdsContent, document.getElementById('mobileCKAds'));
        // console.log('mobileAdsLayout:', mobileAdsLayout)

        creatAdsVars = {
            'adsWrap': document.querySelector('.ads_wraper'),
            'adsImg': document.querySelector('.ads_img'),
            'adsContent': document.querySelector('.ads_content'),
            'btnAdsDisplay': document.querySelector('.btn_ads_display'),
            'btnAdsClose': document.querySelector('.btn_ads_close')
        };
    }

    var setAdsImg = function(){
        // set adsImgBg, adsUrl, but hidden first
        creatAdsVars.adsImg.setAttribute('style', 'background-image: url(' + adsImgBg + ')');
        creatAdsVars.adsImg.setAttribute('onclick', 'window.open("' + adsUrl + '", "_blank")');
        if(adsType !== 'videoAds'){
            var imgWrap = document.querySelector('.adsYtPlayerWrap')
            imgWrap.setAttribute('style', 'background-image: url(' + adsImgBgs + ')');
            imgWrap.setAttribute('onclick', 'window.open("' + adsUrl + '", "_blank")');
        }
        // check reply
        var floatReplyCon = document.getElementById('floatReply-container') ? 1 : 0 ;

        if(!floatReplyCon){
            document.querySelector('.ads_wraper').style.bottom = "0px";
            document.querySelector('.adsYtPlayerWrap').style.bottom = "0px";
        }else{
            document.querySelector('.ads_wraper').style.bottom = "55px";
            document.querySelector('.adsYtPlayerWrap').style.bottom = "55px";
            creatAdsVars.adsImg.style.bottom = "55px";
        }
    }

    var setAdsVideo = function(){

        // set youtube player first
        //第一次播放變數
        var player;
        var firstPlay = true;

        window['onYouTubeIframeAPIReady'] = function() {
            player = new YT.Player('adsYtPlayer', {
                width: '100%',
                height: '100%',
                videoId: adsYtVideoId,
                playerVars: {
                    'playsinline': 1,
                    'enablejsapi': 1,
                    'autoplay': 1,
                    'modestbranding': 1,
                    'fs':0,
                    'rel': 0,
                    'showinfo': 0,
                    'controls': 1,
                },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
            // console.log('player03', player);
        }

        function onPlayerReady(){
            player.mute();
            player.playVideo();
        }

        var setGaReportInterval;

        function onPlayerStateChange(e){
            // ga report (start, end)
            // console.log(e.data)
            switch(e.data){
                case 1:
                    if(firstPlay){
                        // ga('send', 'event', '手機影音蓋板', '播放');
                        console.log('開始播放');
                        firstPlay = false;
                    }
                    setGaReportInterval = window.setInterval(gaReport, 500);
                    break;
                case 0:
                    // ga('send', 'event', '手機影音蓋板', '播放完成');
                    console.log('播放完成');
                case 3:
                case 2:
                    window.clearInterval(setGaReportInterval);
                    break;
            }
            // console.log('全部player.getDuration():', player.getDuration())
            // console.log('目前player.getCurrentTime():', player.getCurrentTime())
        }

        // ga report (duration)
        var pers25 = true,
            pers50 = true,
            pers75 = true;

        function gaReport(){
            var totalTime = player.getDuration() / 100,
                currentTime = Math.floor(player.getCurrentTime() / totalTime);
            console.log('%:', currentTime + '%');
            if(currentTime >= 25 && pers25){
                console.log('25% ga：',currentTime);
                // ga('send', 'event', '手機影音蓋板', '播放至 25%');
                pers25 = false;
            }else if(currentTime >= 50 && pers50){
                console.log('50% ga：',currentTime);
                // ga('send', 'event', '手機影音蓋板', '播放至 50%');
                pers50 = false;
            }else if(currentTime >= 75 && pers75){
                console.log('75% ga：',currentTime);
                // ga('send', 'event', '手機影音蓋板', '播放至 75%');
                pers75 = false;
            }
        }

        // than set youtube api link
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    var showAdsAni = function(){

        var addVisible = false;
        window.addEventListener('scroll', function(){
            if(!addVisible){
                if(creatAdsVars.adsWrap && !creatAdsVars.adsWrap.classList.contains('aniAdsFadeIn')){
                    creatAdsVars.adsWrap.classList.add('aniAdsFadeIn');
                }
                // init youtube player
                if(adsType == 'videoAds'){
                    setAdsVideo();
                }
                // set img, url
                setAdsImg();
            }
            addVisible = true;

            // show btn controls
            setTimeout(function(){
                creatAdsVars.btnAdsDisplay.style.visibility = 'visible';
                creatAdsVars.btnAdsClose.style.visibility = 'visible';
            },2000);
        });
    }

    var btnClose = function(){
        creatAdsVars.adsWrap.setAttribute('style', 'display: none');
    }

    var btnCheckWrap = function(){
        var hasDisplayClass = creatAdsVars.adsWrap.classList.contains('displayAdsFull');
        var imgWrap = document.querySelector('.adsYtPlayerWrap');

        if(hasDisplayClass){
            creatAdsVars.adsWrap.classList.remove('displayAdsFull');
            if(imgWrap.style.backgroundImage){
                imgWrap.style.opacity = '1';
            }
        }else{
            creatAdsVars.adsWrap.classList.add('displayAdsFull')
            if(imgWrap.style.backgroundImage){
                imgWrap.style.opacity = '0';
            }
        }
    }

    var init = function(){
        checkAdsType();
        creatAdsLayout();
        showAdsAni();

        creatAdsVars.btnAdsClose.addEventListener('click', btnClose);
        creatAdsVars.btnAdsDisplay.addEventListener('click', btnCheckWrap);

        console.log(adsImgBg +' : ' + adsUrl +' : ' + adsYtVideoId);
    };
    return init();

};

if (!!window['mobileCKAdsCall'] && typeof window['mobileCKAdsCall'] === 'function') {
    window['mobileCKAdsCall']();
}
